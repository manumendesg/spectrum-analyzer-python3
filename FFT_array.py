### Author : Manuela and Patrick
### Date : 27/10/2022 
### Spectrum Analyzer


import epics
import matplotlib.pyplot as plt


# 1- Constructing array from sin_wave generator in EPICS

pvname = 'calc_sin:sin_wave.VAL' # getting the data
pv = epics.PV(pvname) 

a = [0]*200                       #initialize a zero array of len 200
oldPvValue = 0



#def arrayFunc():




while True:
   while a[199] == 0:             # checking the last value of the list  
      if pv.value != oldPvValue:  # check if the current value has changed from the previous 
         a.insert(0,pv.value)     # if the value changed, insert it to the first location on the list
        # print(a)
         oldPvValue = pv.value    # setting the oldvalue to the current value
   a = a[:-200]                   # deleting the last 200 array locations (zeros)
   print("new array: ",a)         # final value we want to pass to the FFT
   print("clearing array")
   a = []                         # clear the array
   a = [0]*200                    # initialize array again with len 200 and all 0 


   # trying to print the data

plt.plot([1,2,3])
plt.ylabel("frequency")
plt.xlbel("time")
plt.show()

# 2- FFT  


