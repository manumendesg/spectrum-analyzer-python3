### Author : Manuela and Patrick
### Date : 27/10/2022 
### Spectrum Analyzer


import epics
import matplotlib.pyplot as plt
import time
import numpy as np

# 1- Constructing array from sin_wave generator in EPICS

pvname = 'calc_sin:sin_wave.VAL' # getting the data
pv = epics.PV(pvname) 

oldPvValue = 0          
a = []                  # initializing list


# plot settings
t = np.linspace(0, 4, 100)     # creating initial data values for the x-axis
#y = np.linspace()
plt.ion()

figure, ax = plt.subplots(figsize=10,8)
line1 = ax.plot()
plt.title("IOC Sin wave plot")


while True:
   while len(a) < 199:            # checking the last value of the list  
      #a[0] = pv.value
      if pv.value != oldPvValue:  # check if the current value has changed from the previous 
         a.insert(0,pv.value)     # if the value changed, insert it to the first location on the list
         print(len(a))
         oldPvValue = pv.value    # setting the oldvalue to the current value
   print("new array: ",a)         # final value we want to pass to the FFT
  
   plt.draw()
  
   print("clearing array")
   a = []                         # clear the array



plt.plot([1,2,3])
plt.ylabel("frequency")
plt.xlbel("time")
plt.show()

# 2- FFT  
# probably use fftn - compute N dimentional discrete fourier transform (numpy)

